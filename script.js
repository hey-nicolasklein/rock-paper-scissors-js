let playerScore = 0;
let computerScore = 0;


// Possible results. For better random selection each possible result
// has been added twice. 
let options = ['Rock','Paper','Scissors',
                'Rock','Paper','Scissors'];

// Selects a random integer value in the range [0,max[
function getRandomInt(max) {
    return Math.floor(Math.random() * max);
}

// Generates a computer play result
function computerPlay(){
    return options[getRandomInt(6)];
}

// Converts the String representing Rock, Paper or Scissors
// to the corresponding numbers in the range of [0,3[
// The logic is case-insentive.
function selectionToNum(playerSelection){
    playerSelection = playerSelection.toLowerCase();
    switch (playerSelection){
        case 'rock':
            return 0;
            break;
        case 'paper':
            return 1;
        case 'scissors':
            return 2;
        default:
            return -1;
    }
}

// Converts the numeric selection in the range [0,3[
// to Strings representing Rock, Paper and Scissors.
function numToSelection(playerSelection){
    switch (playerSelection){
        case 0:
            return 'Rock';
            break;
        case 1:
            return 'Paper';
        case 2:
            return 'Scissors';
        default:
            return 'Value has to be between 0 and 2.';
    }
}

// Selects the winner based on the numeric input of
// the computer and player round.
function selectWinner(playerRound, computerRound){
    // [player][computer]
    let possibleResult = [
        ['draw', 'computer', 'player'],
        ['player', 'draw', 'computer'],
        ['computer', 'player', 'draw']
    ]
    return possibleResult[playerRound][computerRound];
}


function switchIcon(owner, newIcon){
    let url = '';
    switch(newIcon){
        case 0:
            url = 'resources/rock.png'
            break;
        case 1:
            url = 'resources/paper.png'
            break;
        case 2:
            url = 'resources/scissors.png'
            break;
    }

    //document.getElementsByClassName('scoreImage').forEach(e => e.classList.add('hand'));
    

    if(owner == 'player'){
        document.getElementsByClassName('scoreImage player')[0].setAttribute('src', url);
    }
    else{
        document.getElementsByClassName('scoreImage computer')[0].setAttribute('src', url);
    }

}

// Plays a round of Rock, Paper, Scissors against the computer.
function playRound(playerSelection){
    console.log('New Round!!')

    let playerRound = selectionToNum(playerSelection);
    switchIcon('player', playerRound);
    console.log('User Plays: '+numToSelection(playerRound));

    let computerRound = selectionToNum(computerPlay());
    switchIcon('computer', computerRound);
    console.log('Computer Plays: '+numToSelection(computerRound));

    let winner = selectWinner(playerRound, computerRound);
    console.log(winner);
    document.getElementById('lastWinnerHeading').innerHTML = winner;

    refreshScoreboard(winner);

    return winner;
}

function refreshScoreboard(winner){
    if(winner == 'player'){
        playerScore++;
        document.getElementById('playerScore').innerHTML = playerScore;
    }
    else if (winner == 'computer'){
        computerScore++;
        document.getElementById('computerScore').innerHTML = computerScore;

    }
}



document.getElementsByClassName('rock-btn')[0].addEventListener('click', () =>{
    console.log('Winner is: '+playRound('Rock'));
})

document.getElementsByClassName('paper-btn')[0].addEventListener('click', () =>{
    console.log('Winner is: '+playRound('Paper'));
})

document.getElementsByClassName('scissors-btn')[0].addEventListener('click', () =>{
    console.log('Winner is: '+playRound('Scissors'));
})


